'use strict';

/**
 * @ngdoc directive
 * @name grouponDatePickerApp.directive:nbInputSuggestion
 * @description
 * # nbInputSuggestion
 */
angular.module('grouponDatePickerApp')
  .directive('nbInputSuggestion', [function() {
    return {
      restrict: 'A',
      scope: {
        options: '=',
        ngModel: '='
      },
      templateUrl: function(tElement, tAttrs) {
        return tAttrs.path;
      },
      controller: 'SuggestionCtrl',
      link: function(scope, element, attrs) {
        element.children().insertAfter(element);
      }
    };
  }]);
