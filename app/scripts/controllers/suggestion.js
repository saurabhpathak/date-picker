'use strict';

/**
 * @ngdoc function
 * @name grouponDatePickerApp.controller:SuggestionCtrl
 * @description
 * # SuggestionCtrl
 * Controller of the grouponDatePickerApp
 */
angular.module('grouponDatePickerApp')
  .controller('SuggestionCtrl', function($scope, $http) {

    $scope.finalList = [];

    $http.get('/names.json').
    success(function(data) {
      $scope.items = data;
    }).
    error(function(data) {
      console.log(data);
    });

    $scope.addToList = function(item) {
      $scope.finalList.push(item);
    };
  });
