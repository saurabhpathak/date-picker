'use strict';

describe('Directive: nbInputSuggestion', function () {

  // load the directive's module
  beforeEach(module('grouponDatePickerApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<nb-input-suggestion></nb-input-suggestion>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the nbInputSuggestion directive');
  }));
});
